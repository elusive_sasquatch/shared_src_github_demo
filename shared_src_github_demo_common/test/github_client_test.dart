import 'package:shared_src_github_demo_common/shared_src_github_demo_common.dart';
import 'package:test/test.dart';
import 'dart:convert';

import 'github_search_repositories_demo_json.dart';

void main() {
  group('GithubClient Test Group', () {  
    GithubClient client;

    setUp(() {
      client = GithubClient();
    });

    group('GithubClient should not be null and have default values', () {
      test('GithubClient.httpClient should not be null', () {
        expect(client.httpClient, isNotNull);
      });
      test('GithubClient.baseUrl should not be empty if no baseUrl is provided', () {
        expect(client.baseUrl, isNotEmpty);
      });
    });
  });
}