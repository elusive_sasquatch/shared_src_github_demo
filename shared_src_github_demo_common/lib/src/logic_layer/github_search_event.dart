import 'package:equatable/equatable.dart';

//abstract SearchEvent base class
abstract class GithubSearchEvent extends Equatable {
  GithubSearchEvent([List props = const[]]) : super(props);
}

//text changed event that derives from SearchEvent base class
// members: text changed

class TextChanged extends GithubSearchEvent {
  final String text;

  TextChanged({this.text}) : super([text]);

  @override
  String toString() => 'TextChanged {text: $text}';

}