import 'package:equatable/equatable.dart';

import 'package:shared_src_github_demo_common/shared_src_github_demo_common.dart';

abstract class GithubSearchState extends Equatable {
  GithubSearchState([List props = const []]) : super(props);
}

// SearchStateEmpty
class SearchStateEmpty extends GithubSearchState {
  @override
  String toString() => 'SearchStateEmpty';
}
// SearchStateLoading
class SearchStateLoading extends GithubSearchState {
  @override
  String toString() => 'GithubSearchState';
}

// SearchStateSuccess
class SearchStateSuccess extends GithubSearchState {
  final List<SearchResultItem> items;

  SearchStateSuccess(this.items) : super([items]);

  @override
  String toString() => 'SearchStateSuccess { items: ${items.length} }';
}

// SearchStateError
class SearchStateError extends GithubSearchState {
  final String error;

  SearchStateError(this.error) : super([error]);

  @override
  String toString() => 'SearchStateError';
}
