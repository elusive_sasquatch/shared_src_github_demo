import 'package:shared_src_github_demo_common/shared_src_github_demo_common.dart';

class SearchResultItem {
    final String fullName;// full name of repository
    final String htmlUrl;// html url of repo
    final GithubUser owner;// owner of repo L GithubUser()

    const SearchResultItem({this.fullName, this.htmlUrl, this.owner});

    //static fromJson() constructor
    static SearchResultItem fromJson(Map<String, dynamic> json) {
        final String fullname = json["full_name"] ?? "unkown_fullname";
        final String url = json["html_url"] ?? "unkown_html_url";
        final owner = GithubUser.fromJson(json["owner"] as Map<String, dynamic>);
        
        return SearchResultItem(fullName: fullname, htmlUrl: url, owner: owner);
    }
} 