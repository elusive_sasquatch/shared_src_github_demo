import 'package:shared_src_github_demo_common/shared_src_github_demo_common.dart';

class SearchResult {
  //member:
    final List<SearchResultItem> items;// list of SearchResultItems

    const SearchResult({this.items});

  //should have a static fromJson(someJsonHere) constructor
  static SearchResult fromJson(Map<String,dynamic> json) {
    final items = (json['items'] as List<dynamic>)
      .map( (dynamic item) => SearchResultItem.fromJson(item as Map<String, dynamic>))
      .toList();

    return SearchResult(items: items);
  }
}