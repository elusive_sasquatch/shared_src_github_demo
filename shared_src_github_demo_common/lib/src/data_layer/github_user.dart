/*
"owner": {
        "login": "dtrupenn",
        "id": 872147,
        "node_id": "MDQ6VXNlcjg3MjE0Nw==",
        "avatar_url": "https://secure.gravatar.com/avatar/e7956084e75f239de85d3a31bc172ace?d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png",
        "gravatar_id": "",
        "url": "https://api.github.com/users/dtrupenn",
        "received_events_url": "https://api.github.com/users/dtrupenn/received_events",
        "type": "User"
      }
*/

class GithubUser {
  final String login;
  final int id;
  final String avatarUrl;
  final String url;

  const GithubUser({this.login, this.id, this.avatarUrl, this.url});

  //from Json
  static GithubUser fromJson(Map<String, dynamic> json) {
    final String login = json["login"] ?? "unknown_login";
    final int id = json["id"] ?? -1;
    final String avatarUrl = json["avatar_url"] ?? "unkown_avatar_url";
    final String url = json["url"] ?? "unkown_url";

    return GithubUser(login: login, id: id, avatarUrl: avatarUrl, url: url);
  }
}