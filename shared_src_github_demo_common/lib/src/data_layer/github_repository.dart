import 'dart:async';

import 'package:shared_src_github_demo_common/shared_src_github_demo_common.dart';


class GithubRepository {
  final GithubCache cache;
  final GithubClient client;

  GithubRepository(this.cache, this.client);

  Future<SearchResult> search(String term) async {
    if (cache.containsSearch(term)) {
      return cache.getSearch(term);
    }
    else {
      final result = await client.search(term);
      cache.addSearch(term, result);
      return result;
    }
  }
}