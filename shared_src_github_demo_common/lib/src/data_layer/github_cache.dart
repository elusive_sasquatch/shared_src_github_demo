import 'package:shared_src_github_demo_common/shared_src_github_demo_common.dart';

class GithubCache {
    final Map<String, SearchResult> _cache;

    GithubCache({GithubCache cache}) 
    : _cache = cache ?? new Map<String, SearchResult>(); 

    bool containsSearch(String term) => _cache.containsKey(term);
    void addSearch(String term, SearchResult result) => _cache[term] = result;
    SearchResult removeSearch(String term) => _cache.remove(term);
    SearchResult getSearch(String term) => _cache[term];

}