class SearchResultError {
  final String message;

  SearchResultError({this.message});

  static SearchResultError fromJson(Map<String, dynamic> json) {
    final String msg = json["message"] ?? "unknown_message";
    return SearchResultError(message: msg);
  }

}